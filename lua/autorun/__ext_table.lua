//
// Table Extension - Josh 'Acecool' Moser
//


//
//
//
function table.tojson( _table )
	if ( isstring( _table ) ) then return _table; end

	return util.TableToJSON( _table );
end


//
//
//
function table.fromjson( _string )
	if ( istable( _string ) ) then return _string; end

	return util.JSONToTable( _string );
end


//
// Calculates table size...
//
function table.size( _table )
	local _tabletostring = table.tojson( _table );
	return string.len( _tabletostring ), _tabletostring;
end


//
// Splits a table into smaller chunks
//
function table.chunk( _table, _divisor )
	local _size, _data = table.size( _table );

	local _chunks = { };

	// Sending size supports 64kb, I'm doing 63kb chunks to support headers to rebuild the chunks.
	local _count = math.ceil( _size / _divisor );
	for i = 1, _count do
		-- print( string.sub( _data, 1 + ( _divisor * i ) - _divisor, _divisor * i ) )
		table.insert( _chunks, string.sub( _data, 1 + ( _divisor * i ) - _divisor, _divisor * i ) )
	end

	return _chunks, _count;
end