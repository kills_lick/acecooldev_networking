//
// Convers a lot of the NW vars into Flags ( meaning server won't update client unless var changes ) - Josh 'Acecool' Moser
//
local FLAG_NWVARS = "nwvars";

local function NWSetter( self, _key, _value )
	self:SetFlag( _key, _value, false, FLAG_NWVARS );
end

local function NWGetter( self, _key, _default )
	return self:GetFlag( _key, _default, false, FLAG_NWVARS );
end

META_ENTITY.SetNWAngle = NWSetter;
META_ENTITY.GetNWAngle = NWGetter;
META_ENTITY.SetNetworkedAngle = NWSetter;
META_ENTITY.GetNetworkedAngle = NWGetter;

META_ENTITY.SetNWBool = NWSetter;
META_ENTITY.GetNWBool = NWGetter;
META_ENTITY.SetNetworkedBool = NWSetter;
META_ENTITY.GetNetworkedBool = NWGetter;

META_ENTITY.SetNWEntity = NWSetter;
META_ENTITY.GetNWEntity = NWGetter;
META_ENTITY.SetNetworkedEntity = NWSetter;
META_ENTITY.GetNetworkedEntity = NWGetter;

META_ENTITY.SetNWFloat = NWSetter;
META_ENTITY.GetNWFloat = NWGetter;
META_ENTITY.SetNetworkedFloat = NWSetter;
META_ENTITY.GetNetworkedFloat = NWGetter;

META_ENTITY.SetNWInt = NWSetter;
META_ENTITY.GetNWInt = NWGetter;
META_ENTITY.SetNetworkedInt = NWSetter;
META_ENTITY.GetNetworkedInt = NWGetter;

META_ENTITY.SetNWString = NWSetter;
META_ENTITY.GetNWString = NWGetter;
META_ENTITY.SetNetworkedString = NWSetter;
META_ENTITY.GetNetworkedString = NWGetter;

META_ENTITY.SetNWVarProxy = NWSetter;
META_ENTITY.GetNWVarProxy = NWGetter;
META_ENTITY.SetNetworkedVar = NWSetter;
META_ENTITY.GetNetworkedVar = NWGetter;

META_ENTITY.SetNWVector = NWSetter;
META_ENTITY.GetNWVector = NWGetter;
META_ENTITY.setNetworkedVector = NWSetter;
META_ENTITY.GetNetworkedVector = NWGetter;

META_ENTITY.NetworkVar = function( self, _type, _slot, _name, _extended )
	local _key = _extended.KeyName;
	self[ "Set" .. _name ] = function( self, _value )
		NWSetter( self, _key, _value );
	end

	self[ "Get" .. _name ] = function( self, _default )
		return NWGetter( self, _key, _default );
	end
end