//
// Networking - Josh 'Acecool' Moser
//

--[[

	NOTE: This isn't the final version of this networking system. I'd highly recommend not converting it
	to gmod_standards ( zerf ) or making any modifications until the final version is up.

	This is just a basic ( old ) version which doesn't have all of the new features because that one is
	taking a little longer than anticipated for me to finish because of my health problems. However, I
	wanted to get something out to you which will work so this will work for now.

	Default setup has splitting packets larger than 10KB into separate messages. It is configurable.
	Default setup doesn't have a queue meaning messages are sent first-come-first-send which can
		cause issues if you send a lot of data regularly ( screenshots for example ) which prevents
		other data from being received until that packet is done sending ( net message limitation ).

	Enjoy the current release!

--]]


//
// Networking based ENUMs
//
NET_CLIENT = 0;
NET_SERVER = 1;
NET_SHARED = 2;

// This is how fast the engine can transfer data per second
NETWORKING_ENGINE_TRANSMISSION_LIMIT		= SIZE_KB * 20;

// Maximum size to allow without requiring queue
NETWORKING_TRANSMISSION_QUICK_SEND_SIZE		= SIZE_KB * 5;

// This is how much data we want to transfer per packet.
NETWORKING_TRANSMISSION_RATE				= SIZE_KB * 10;

// Delay added between sending packets to allow other messages to be sent - added on top of time taken to transfer
NETWORKING_TRANSMISSION_PACKET_DELAY_TIME	= 0.25;//1.25;

//
NETWORKING_MESSAGE_NAME = "__NETWORKING_CHUNKS__";


//
//
//
if ( !networking ) then
	networking = {
		[ NET_SHARED ] = {
			// Global Data Recognition Rules
			// If a datatype isn't passed in to match a rule, a notice is thrown and fallback networking is used ( WriteTable )
			arguments_table = { };

			// Global Data Conversion Rules
			// Examples:
			//	[ number -> string -> number ]
			//	[ Entity || number -> number -> Entity ]
			conversions_table = {
				[ TYPE_NUMBER ] = {
					to = function( _data ) return tostring( _data ); end;
					from = function( _data ) return tonumber( _data ); end
				};
				[ TYPE_ENTITY ] = {
					to = function( _data )
						if ( IsValid( _data ) ) then
							return _data:GetID( );
						elseif ( isnumber( _data ) ) then
							return _data;
						end

						return -1;
					end;

					from = function( _data )
						return _data;
						//return Entity( _data );
					end
				};
			};

			//
			syncable_flags = { };

			// Per Receiver Send / Receiving Rules ( including conversion )
			functions = { };
		};

		[ NET_CLIENT ] = {
			// Realm ID
			name		= "CLIENT";

			// Networking Functions
			functions	= { };

			// Packets in Receiver
			packets		= { };

			// The current queue, sorted by size so smaller messages get highest priority
			queue		= { };

			// How many messages currently in the queue ( enable sender hook on 1, end sender hook on 0 )
			count		= 0;
		};

		[ NET_SERVER ] = {
			// Realm ID
			name		= "SERVER";

			// Networking Functions
			functions	= { };

			// Packets in Receiver
			packets		= { };

			// The current queue, sorted by size so smaller messages get highest priority
			queue		= { };

			// How many messages currently in the queue ( enable sender hook on 1, end sender hook on 0 )
			count		= 0;
		};
	};
	networking.__index = networking;
end


//
//
//
networking.__config = {
	// Turns key-names into numbers in order to send/rcv less data ( Requires key/data-table to be sent and managed )
	optimization_mode				= false;

	// Should players that are connecting receive data ( if large amount is sent prior to fully-connected, can cause snapshot overflow, crash, etc... )
	transmit_to_connecting_players	= false;

	// If the packets should be staggered ( wait until large packet fully sent at engine transmission limit before sending next packet )
	packet_stagger					= true;

	// Delay between large packets being sent ( added to time it takes to send at engine transmission limit if stagger is enabled )
	packet_buffer_delay				= 1.25;

	// Should messages that are split up be verified ( not needed unless using timers )
	packet_receipt					= false;
};


//
// Helper function which will returns the current realm table and player object
//
function networking:GetRealm( _p )
	// Realm
	local _realm = networking[ NET_SERVER ];
	if ( CLIENT ) then
		_realm = networking[ NET_CLIENT ];
		_p = LocalPlayer( );
	end

	return _realm, _p;
end


//
// Registers a receiver arguments so that instead of writing/reading a table, it'll use the correct
// functions and won't require a table with data-types ( this NEEDS to be called in SHARED environment )
// Unless the data size is too large; then it'll use the standard transmission...
//
function networking:RegisterReceiver( _name, _args )
	networking[ NET_SHARED ].functions[ _name ] = {
		// Arguments to send and receive data with
		args			= ( _args && istable( _args ) ) && _args || { };
	};
end


//
// Returns true if the the receiver is registered along with the args table, false and empty table
//
function networking:IsRegistered( _name )
	local _data = networking[ NET_SHARED ].functions[ _name ];
	if ( _data ) then
		return true, _data;
	end

	return false;
end


//
// A shared receiver can be created, depending on if the client sends to server or the server sends
// to client will affect the realm where it's executed; make sure you understand realms when
// adding shared receivers. IE shared code doesn't execute on both client and server at the same time,
// if you add a shared receiver and send from client to server for it to execute then it'll execute on
// the server only. If you send from server to client( s ) then it'll execute on the clients. If you want
// it to be as close to "shared" as possible then when you send it from either client/server, use the
// built-in networking:Call function to have it execute on the current realm and send it to the other
// realm to execute there too. I should actually write a built-in thing for that....
// -- The better way is to add receivers on the client or the server in a specific file.
//
function networking:AddReceiver( _name, _func )
	// Realm
	local _realm, _p = self:GetRealm( _p );

	// New way, either send table with unknown arguments, or write/read exactly what they are.
	_realm.functions[ _name ] = {
		func = _func;
	};

	-- debug.print( "Networking:AddReceiver", "Receiver: " .. _name .. " added to realm: " .. _realm.name );
end


//
// Receivers can be called like functions by using the Call function.
//
function networking:Call( _name, _p, ... )
	// Realm
	local _realm, _p = self:GetRealm( _p );

	local _data = _realm.functions[ _name ];
	if ( _data && istable( _data ) && isfunction( _data.func ) ) then
		_data.func( _p, ... );
	else
		debug.print( "Networking:UnhandledCall", "Unhandled " .. _realm.name .. " networking receiver: " .. _name .. " - sent by " .. _p .. "\n", ... );
	end
end


//
//
//
function networking:SyncFlag( _name, _p, _ent, _value, _private, _category )
	if ( CLIENT ) then
		_ent:SetFlag( _name, _value, _private, _category );
		networking:SendToServer( "SyncFlag", _ent, _name, _value, _private, _category );
	else
		networking:Call( "SyncFlag", _p, _ent, _name, _value, _private, _category );
	end
end


//
// This is the internal function to send data from the queue block from Send...
//
function networking:Send( _name, _players, ... )
	// Realm
	local _realm, _p = self:GetRealm( _p );

	// Conversion TO code
	// Process data to be sent :: Potentially add an AddProcessData function so that data types can be wrapped and unwrapped..
	// This fixes the number bug, in addition to ensuring that Entities / Players, etc can be transmitted.
	local _table = { ... };
	local _tableMap = { };
	for k, v in pairs( _table ) do
		_tableMap[ k ] = TypeID( v );
		if ( _tableMap[ k ] == TYPE_ENTITY ) then
			_table[ k ] = v:EntIndex( );
		elseif ( _tableMap[ k ] == TYPE_NUMBER ) then
			_table[ k ] = tostring( v );
		end
	end

	// Net-code
	local _packets = { };
	local _size, _data = table.size( _table );
	local _chunks, _packetCount = table.chunk( _data, NETWORKING_TRANSMISSION_RATE );
	local _messageID = ( _packetCount > 1 ) && GenerateMessageID( ) || nil;
	_packetCount = ( _packetCount > 1 ) && _packetCount || nil;

	//
	for k, v in pairs( _chunks ) do
		local _packet = {
			message_id = _messageID;
			packet = ( ( _packetCount ) && k || nil );
			packet_count = _packetCount;
			packet_data = v
		};
		table.insert( _packets, _packet );

		// More than 1 packet, send on a variable timer; next-frame for first message with 0 timer, then next message every x seconds.
		if ( _packetCount ) then
			// 20kB / sec speed when sending, each packet is 63kB... 3 seconds per packet seems reasonable.
			local _timer = ( ( NETWORKING_TRANSMISSION_RATE / NETWORKING_ENGINE_TRANSMISSION_LIMIT ) * ( k - 1 ) ) + NETWORKING_TRANSMISSION_PACKET_DELAY_TIME;
			timer.Simple( _timer, function( )
				debug.print( "Networking:SendingPackets", "Sending Message '" .. _name .. "' packet " .. k .. " of " .. _packet_count .. "!" );
				self:__NetSender( _name, _tableMap, _packet, _players );
			end );
		else
			// No need for a timer when sending 1 packet.
			self:__NetSender( _name, _tableMap, _packet, _players );
		end
	end
end


//
// Internal Helper-Function
// Cuts down on a ton of repeated code for the two child-events: Multiple-packets vs 1 Packet.
//
function networking:__NetSender( _name, _map, _packet, _players )
	// Realm Information
	local _registered, _registered_map = self:IsRegistered( _name );
	local _realm, _p = self:GetRealm( _p );

	// Start the message ( It doesn't matter if there is one message name used, or 50; they'll be sent the same... )
	net.Start( NETWORKING_MESSAGE_NAME );
		net.WriteString( _name );
		net.WriteTable( _map );
		net.WriteTable( _packet );

	// Realm converter
	if ( SERVER ) then
		if ( _players && ( _players != NULL || istable( _players ) ) ) then
			if ( networking.__config.transmit_to_connecting_players ) then
				if ( !istable( _players ) && IsValid( _players ) && _players:IsFullyConnected( ) ) then
					net.Send( _players );
				else
					debug.print( "Networking:IgnoringUnconnectedPlayer", "Not sending data to unconnected player..." .. _players );
				end
			else
				net.Send( _players );
			end
		else
			if ( networking.__config.transmit_to_connecting_players ) then
				net.Broadcast( );
			else
				// Ensures connecting players don't get flooded; they'll be synced on connect anyway, no need for info before then.
				net.Send( player.GetAllConnected( ) );
			end
		end
	else
		net.SendToServer( );
	end
end


//
// Internal Helper-Function
// Handles Receiving of data
//
function networking:Receiver( bytes, _p, _name, _map, _data )
	// Realm
	local _realm, _p = self:GetRealm( _p );

	// Packet info - count, and number in sequence..
	local _packet 		= ( _data.packet ) && _data.packet || 1;
	local _packet_count = ( _data.packet_count ) && _data.packet_count || 1;
	local _packet_data 	= _data.packet_data;
	local _packet_id 	= _data.message_id;

	if ( _packet_count > 1 ) then
		if ( !_realm.packets[ _packet_id ] ) then
			_realm.packets[ _packet_id ] = { };
		end

		_realm.packets[ _packet_id ][ _packet ] = _packet_data;
	end

	if ( _realm.packets[ _packet_id ] ) then
		if ( table.Count( _realm.packets[ _packet_id ] ) < _packet_count ) then
			debug.print( "Networking:WaitingOnPackets", "Not all packets arrived yet! Currently on " .. _data.packet .. " of " .. _packet_count .. "!" );
			return;
		else
			debug.print( "Networking:PacketsReceived", "All packets have arrived. Reassembling data!" );

			_packet_data = "";
			for k, v in pairs( _realm.packets[ _packet_id ] ) do
				_packet_data = _packet_data .. v;
				_realm.packets[ _packet_id ][ k ] = nil;
			end

			_realm.packets[ _packet_id ] = nil;
		end
	end

	//
	local _tab = table.fromjson( _packet_data );

	// Process mapped data - this is done to fix the number glitch, in addition to ensuring passed Entities are entities on arrival.
	for k, v in pairs( _tab ) do
		if ( _map[ k ] == TYPE_ENTITY ) then
			_tab[ k ] = Entity( v );
		elseif ( _map[ k ] == TYPE_NUMBER ) then
			_tab[ k ] = tonumber( v );
		end
	end

	//
	if ( _realm.functions[ _name ] && istable( _realm.functions[ _name ] ) && isfunction( _realm.functions[ _name ].func ) ) then
		_realm.functions[ _name ].func( _p, unpack( _tab ) ); //unpack( args ) );
	else
		if ( SERVER ) then
			debug.print( "Networking:UnhandledMessage", "Unhandled " .. _realm.name .. " networking receiver: " .. _name .. " - sent by " .. _p );
		else
			debug.print( "Networking:ClientUnhandledMessage", "Unhandled " .. _realm.name .. " networking receiver: " .. _name .. " - sent by " .. _p );
			networking:Send( "__UNHANDLED_MESSAGE__", nil, _name, unpack( _tab ) );
		end
	end
end


//
//
//
net.Receive( "__NETWORKING_CHUNKS__", function( _len, _p )
	local _name = net.ReadString( );
	local _map = net.ReadTable( );
	local _data = net.ReadTable( );

	-- local _lensize, _lenkb = math.ConvertSize( _len / 8, SIZE_KB );
	networking:Receiver( _len, _p, _name, _map, _data );
end );


//
// Simple Hook for adding networking:AddReceiver in.
//
hook.Call( "PostInitNetworking", ( GM || GAMEMODE ) );